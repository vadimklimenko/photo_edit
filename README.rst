==========
Photo edit
==========

Quick start
-----------

1. Add "photo_edit" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'photo_edit',
    ]

2. Include the polls URLconf in your project urls.py like this::

    url(r'^products/photo_edit/', include('photo_edit.urls')),

3. Visit http://127.0.0.1:8000/products/photo_edit/ to run demo.