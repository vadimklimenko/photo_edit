from django.conf.urls import url
from photo_edit.views import ProcessPhotoTemplateView

urlpatterns = {
    url(r'^$', ProcessPhotoTemplateView.as_view(), name='process_photo'),
}
