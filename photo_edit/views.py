from __future__ import unicode_literals

import json

from PIL import Image
from django.http import HttpResponse, JsonResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt


class ProcessPhotoTemplateView(generic.TemplateView):
    template_name = 'photo_edit/process_photo_template.html'

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(ProcessPhotoTemplateView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        image = Image.open(self.request.FILES['image'])
        original_image_sizes = image.size

        operations = json.loads(self.request.POST['operations'])

        is_need_postresize = False
        # is_need_postresize used if we scaled image and then rotate it
        # For example: original size 100x100, scaled to 90x90
        # And we will rotate 90x90 image, not 100x100

        for operation in operations:
            if operation['action'] == "rotate":
                image = image.rotate(operation['rotate_degree'], expand=True, resample=Image.NEAREST)

                is_need_postresize = False

            if operation['action'] == "shift":
                image_width = image.size[0]
                image_height = image.size[1]

                shift_up = shift_left = None

                try:
                    shift_up = int(operation['shift_up'])
                except KeyError:
                    pass

                try:
                    shift_left = int(operation['shift_left'])
                except KeyError:
                    pass

                image_size = [0, 0, image_width, image_height]

                try:
                    if shift_up:
                        if shift_up > 0:
                            image_size[1] += shift_up
                        elif shift_up < 0:
                            image_size[3] += shift_up
                except SystemError:
                    return JsonResponse({
                        'error_code': 0,
                        'error_text': 'Incorrect shift_up value for operation {0}'.format(operation['action']),
                        'error_description': 'shift_up cannot extend outside image'
                    })

                try:
                    if shift_left:
                        if shift_left > 0:
                            image_size[0] += shift_left
                        elif shift_left < 0:
                            image_size[2] += shift_left
                except SystemError:
                    return JsonResponse({
                        'error_code': 0,
                        'error_text': 'Incorrect shift_left value for operation {0}'.format(operation['action']),
                        'error_description': 'shift_left cannot extend outside image'
                    })

                image = image.crop(image_size)

                is_need_postresize = False

            if operation['action'] == "scale":
                original_image_sizes = image.size
                try:
                    scale_perc = float(operation['scale_perc']) * 0.01
                except (KeyError, ValueError):
                    return JsonResponse({
                        'error_code': 1,
                        'error_text': 'Incorrect scale_perc value'
                    })

                image_width = int(image.size[0] * scale_perc)
                image_height = int(image.size[1] * scale_perc)

                image = image.resize((image_width, image_height), Image.ANTIALIAS)

                is_need_postresize = True

        if is_need_postresize:
            image = image.crop([0, 0, original_image_sizes[0], original_image_sizes[1]])

        response = HttpResponse(content_type="image/png")
        image.save(response, "PNG")
        return response
